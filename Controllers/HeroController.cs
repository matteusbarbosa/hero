using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using HeroApi.Models;

namespace HeroApi.Controllers
{
    [Route("api/hero")]
    [ApiController]

    public class HeroController : ControllerBase
    {
        private readonly HeroContext _context;

        public HeroController(HeroContext context)
        {
            _context = context;
            if(_context.HeroItems.Count() == 0)
            {
                _context.HeroItems.Add(new HeroItem { Name = "Item1"});
                _context.SaveChanges();
            }
        }

        [HttpGet]
        
        public ActionResult<List<HeroItem>> GetAll()
        {
            return _context.HeroItems.ToList();
        }

        [HttpGet("{id}", Name = "GetHero")]
        public ActionResult<HeroItem> GetById(long id)
        {
            var item = _context.HeroItems.Find(id);
            if(item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
public IActionResult Create(HeroItem item)
{
    _context.HeroItems.Add(item);
    _context.SaveChanges();

    return CreatedAtRoute("GetHero", new { id = item.Id }, item);
}

[HttpPatch("{id}")]
public IActionResult Update(long id, HeroItem item)
{
    var todo = _context.HeroItems.Find(id);
    if (todo == null)
    {
        return NotFound();
    }

    todo.IsComplete = item.IsComplete;
    todo.Name = item.Name;

    _context.HeroItems.Update(todo);
    _context.SaveChanges();
    return NoContent();
}

[HttpDelete("{id}")]
public IActionResult Delete(long id)
{
    var todo = _context.HeroItems.Find(id);
    if (todo == null)
    {
        return NotFound();
    }

    _context.HeroItems.Remove(todo);
    _context.SaveChanges();
    return NoContent();
}


    }
}